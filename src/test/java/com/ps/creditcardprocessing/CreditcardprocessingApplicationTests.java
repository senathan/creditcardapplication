package com.ps.creditcardprocessing;

import com.ps.creditcardprocessing.dto.Card;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest(classes = CreditcardprocessingApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CreditcardprocessingApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void contextLoads() {
	}

	@Test
	public void testToGetAllCardList() {

		ResponseEntity<List> responseEntity = this.restTemplate
				.getForEntity("http://localhost:" + port + "/api/credit/getCardList", List.class);
		Assertions.assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Test
	public void testWithValidCreditCardNumber() {
		Card card = new Card();
		card.setAccountName("RAJA");
		card.setCarLimit(5000);
		card.setNumber("4242424242426742");
		ResponseEntity<Card> responseEntity = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/credit/addCard", card, Card.class);
		Assertions.assertEquals(200, responseEntity.getStatusCodeValue());
	}

	@Test
	public void testIfCreditCardNumberIsEmpty() {
		Card card = new Card();
		card.setAccountName("RAMESH");
		card.setCarLimit(5000);
		card.setNumber(null);
		ResponseEntity<Card> responseEntity = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/credit/addCard", card, Card.class);
		Assertions.assertEquals(400, responseEntity.getStatusCodeValue());
		Assertions.assertEquals("You must enter a credit card number.", responseEntity.getBody().getNumber());
	}

	@Test
	public void testIfCreditCardNumberIsInvalid() {
		Card card = new Card();
		card.setAccountName("TATA");
		card.setCarLimit(6000);
		card.setNumber("4242424242426");
		ResponseEntity<Card> responseEntity = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/credit/addCard", card, Card.class);
		Assertions.assertEquals(400, responseEntity.getStatusCodeValue());
		Assertions.assertEquals("Credit card number should be at least 16 digit", responseEntity.getBody().getNumber());
	}

	@Test
	public void testIfCreditCardNumberIsInvalidWithLuhn() {
		Card card = new Card();
		card.setAccountName("SIVA");
		card.setCarLimit(3400);
		card.setNumber("1111111111111111");
		ResponseEntity<Card> responseEntity = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/credit/addCard", card, Card.class);
		Assertions.assertEquals(400, responseEntity.getStatusCodeValue());
		Assertions.assertEquals("Invalid card number - LUHN ALOG doesn't fit", responseEntity.getBody().getNumber());
	}

}
