package com.ps.creditcardprocessing.dto;

import com.ps.creditcardprocessing.util.CreditCardValidator;

import javax.validation.Constraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.*;


@NotEmpty(message = "You must enter a credit card number.")
@Size(min = 16,max = 19)
@Pattern(regexp="(\\d{19})")
@Documented
@Constraint(validatedBy = CreditCardValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CardValidator {

    String message() default "Invalid card number";
}
