package com.ps.creditcardprocessing.dto;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name="tbl_card")
public class Card implements Serializable {

    @NotEmpty(message = "You must enter a account name.")
    private String accountName;

//    @CardValidator
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @NotBlank(message = "You must enter a credit card number.")
    @Size(message = "Credit card number should be at least 16 digit", max = 19, min = 16)
    @Pattern(regexp="(\\d{16})",message = "Invalid card number")
    private String number;

    private int carLimit;

    private int accountBalance = 0;


    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCarLimit() {
        return carLimit;
    }

    public void setCarLimit(int carLimit) {
        this.carLimit = carLimit;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Override
    public String toString() {
        return "Card{" +
                "accountName='" + accountName + '\'' +
                ", number='" + number + '\'' +
                ", carLimit=" + carLimit +
                ", accountBalance=" + accountBalance +
                '}';
    }
}
