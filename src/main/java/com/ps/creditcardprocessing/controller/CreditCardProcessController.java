package com.ps.creditcardprocessing.controller;

import com.ps.creditcardprocessing.dto.Card;
import com.ps.creditcardprocessing.service.CreditCardProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8091")
@RestController
public class CreditCardProcessController {

    @Autowired
    private CreditCardProcessService creditCardProcessService;

    @RequestMapping(method = RequestMethod.GET, value = "/api/credit/getCardList")
    public ResponseEntity<List<Card>> getAllCardInfo(){
        List<Card> cardList =  creditCardProcessService.getCreditCardList();
        return new ResponseEntity<List<Card>>(cardList,HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value="/api/credit/addCard")
    public ResponseEntity<?> saveCreditCardInfo(@Valid @RequestBody Card card){
        try{
            String status = creditCardProcessService.addOrUpdateCard(card);
            if(status == null){
                return new ResponseEntity<Error>(new Error("Invalid card number - LUHN ALOG doesn't fit"),HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<Card>(HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<Error>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
