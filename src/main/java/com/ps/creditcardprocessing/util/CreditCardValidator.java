package com.ps.creditcardprocessing.util;

import com.ps.creditcardprocessing.dto.CardValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CreditCardValidator implements ConstraintValidator<CardValidator, String> {


    @Override
    public boolean isValid(String cardNumber, ConstraintValidatorContext constraintValidatorContext) {
        return validateCard(cardNumber);
    }

    private boolean validateCard(String cardNumber) {
        int sumOfDigit = 0;

        boolean isEven = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--) {
            int k = Integer.parseInt(String.valueOf(cardNumber.charAt(i)));
            sumOfDigit += sumIfGreaterThan9((k * (isEven ? 2 : 1)));
            isEven = !isEven;
        }

        return (sumOfDigit % 10 == 0);
    }

    private static int sumIfGreaterThan9(int digit) {
        if (digit < 10)
            return digit;
        return sumIfGreaterThan9(digit / 10) + (digit % 10);
    }
}
