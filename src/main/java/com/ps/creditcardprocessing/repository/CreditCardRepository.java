package com.ps.creditcardprocessing.repository;

import com.ps.creditcardprocessing.dto.Card;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<Card, Long> {



}
