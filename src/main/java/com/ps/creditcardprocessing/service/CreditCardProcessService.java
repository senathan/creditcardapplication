package com.ps.creditcardprocessing.service;


import com.ps.creditcardprocessing.dto.Card;
import com.ps.creditcardprocessing.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditCardProcessService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    public List<Card> getCreditCardList() {
        return (List<Card>) creditCardRepository.findAll();
    }


    public String addOrUpdateCard(Card card) {
        if(validateCard(card.getNumber())) {
            creditCardRepository.save(card);
            return HttpStatus.OK.toString();
        }
        return null;
    }

    private boolean validateCard(String cardNumber) {
        int sumOfDigit = 0;

        boolean isEven = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--) {
            int k = Integer.parseInt(String.valueOf(cardNumber.charAt(i)));
            sumOfDigit += sumIfGreaterThan9((k * (isEven ? 2 : 1)));
            isEven = !isEven;
        }

        return (sumOfDigit % 10 == 0);
    }

    private static int sumIfGreaterThan9(int digit) {
        if (digit < 10)
            return digit;
        return sumIfGreaterThan9(digit / 10) + (digit % 10);
    }
}
