import React from 'react';


const Form = props => {
  return (
    <div className="form-group">
      <label> Name </label>
      <input type="text" name="accountName"
             value={props.card.accountName}
             onChange={props.handleInputChange} />
      <label> Card number </label>
      <input type="text" name="number"
             value={props.card.number}
             onChange={props.handleInputChange} />
      <label> Limit </label>
      <input type="number" name="carLimit"
             value={props.card.carLimit}
             onChange={props.handleInputChange} />
      <button onClick={props.addCard}> Add </button>
    </div>
  );
}

export default Form;