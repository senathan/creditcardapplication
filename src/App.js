import React, { useState, useEffect } from "react";
import $ from 'jquery-ajax';
import './bootstrap-material-design.css';
import CreditForm from './components/CreditForm';
import ExtensionList from './components/ExtensionList';

function App() {
  const [card, setCard] = useState({
          accountName: '', number: '', carLimit: 0
  });
  const [extensionList, setExtensionList] = useState([]);

  useEffect(() => {
    $.get("http://localhost:8091/api/credit/getCardList", response => setExtensionList(response));
  }, []);

    const addCard = () => {
//        if (isFormValid()){
          $.ajax({
            url:"http://localhost:8091/api/credit/addCard",
            type:"POST",
            data:JSON.stringify(card),
            contentType:"application/json;charset=utf-8",
            success: function(r){
              extensionList( [...extensionList, card] );
              setCard({accountName: '', number: '', carLimit: ''});
            },
            error: function(xhr, status, error){
              if (xhr.status === 409) {
                alert("A Credit Card with that number already exists!");
              } else {
                alert("A service error occurred, please try again later!");
              }
            }
          });
//        }
      }

  return (
      <div className="App">
        <h1>Credit Card System</h1>

        <CreditForm card={card}
              addCard={addCard} />

        <ExtensionList cards={extensionList} />

      </div>
    );
}

export default App;
